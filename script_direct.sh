#!/bin/bash
catalog=$1;
cd $1;
name_folder='test';
mkdir $name_folder;
cd $name_folder;
echo "Вы выбрали следующий путь для расположения папки test:";
echo $1;
path=`pwd`;
echo "Введите количесво папок на первом уровне:";
read num_folders_level1;
echo "Введите количесво папок на втором уровне:";
read num_folders_level2;
echo "Введите количесво пустых файлов:";
read num_empty_files;
echo "Введите шаблон имени";
read name_pattern;
for (( i=1; i<=$num_folders_level1; i++ ))
do
	mkdir $i;
	cd $i;
	for  (( j=1; j<=$num_folders_level2; j++ ))
	do
		mkdir $j;
		cd $j;
		for (( k=1; k<=$num_empty_files; k++))
		do 
		name_files="$name_pattern$k";
		touch $name_files;
		done
		cd ..;
	done
	cd $path;
done
echo "Скрипт успешно завершен!";
