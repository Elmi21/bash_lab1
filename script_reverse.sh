#!/bin/bash
echo "Выберите пункт меню";
echo "1 - удаление всей директории";
echo "2 - ввод интервала для удаления папок на первом уровне";
echo "3 - ввод интервала для удаления папок на втором уровне";
echo "4 - ввод интрвала для удаления файлов";
echo "5 - ввод определённых папок для удаления на 1 уровне";
echo "6 - ввод определённых папок для удаления на 2 уровне";
echo "7 - ввод определённых файлов для удаления";
read item;
echo "Вы выбрали пункт меню:"
echo $item;
cd $1;
num_folders_level1=$(ls -1d */| wc -l);
echo "Количество папок на первом уровне";
echo $num_folders_level1;
if (($num_folders_level1==0))
then
	echo "Директория пуста! Папок первого уровня нет. Продолжение работы невозможно";
	exit 1;
fi
cd 1;
num_folders_level2=$(ls -1d */| wc -l);
echo "Количество папок на втором уровне";
echo $num_folders_level2;
if (($num_folders_level2==0))
then
	echo "Директория пуста! Папок второго уровня нет. Продолжение работы невозможно";
	exit 1;
fi
cd 1;
num_empty_files=$(ls -1 | wc -l);
echo "Количество файлов";
echo $num_empty_files;
#удаление папки
cd $1;
if (($item==1))
then
rm -rf $1;
echo "Директория была удалена!";
fi
#удаление интервала папок на первом уровне
if (($item==2))
then
echo "Введите левую границу интервала";
read border1;
echo "Введите правую границу интервала";
read border2;
for (( i=$border1; i<=$border2; i++ ))
do
	rm -rf $i;
done
echo "Папки первого уровня были удалены";
fi
#удаление интервала папок на втором уровне
if (($item==3))
then
echo "Введите левую границу интервала";
read border1;
echo "Введите правую границу интервала";
read border2;
cd $1; 
for (( j=1; j<=$num_folders_level1; j++ ))
do
	cd $j;
	for (( i=$border1; i<=$border2; i++ ))
	do
		rm -rf $i;
	done
	cd ..;
done
echo "Папки второго уровня были удалены";
fi
#удаление интервала файлов
if (($item==4))
then
echo "Введите шаблон имени папок:"
read name_folder;
echo "Введите левую границу интервала";
read border1;
echo "Введите правую границу интервала";
read border2; 
cd $1;
for (( j=1; j<=$num_folders_level1; j++ ))
do
	cd $j;
	for (( i=1; i<=$num_folders_level2; i++ ))
	do
		cd $i;
		for (( k=$border1; k<=$border2; k++ ))
		do
			rm -rf $name_folder$k;
		done
		cd ..;
	done
	cd ..;
done
echo "Файлы были удалены";
fi
#удаление конкретных папок на первом уровне
if (($item==5))
then
echo "Введите названия удаляемых папок";
read del_folders;
del_folders=( `echo "$del_folders"` ); 
num_del_folders=${#del_folders[@]};
cd $1;
for (( i=0; i<$num_del_folders; i++ ))
do
	rm -rf ${del_folders[i]};
done
echo "Папки первого уровня были удалены";
fi
#удаление конкретных папок на втором уровне
if (($item==6))
then
echo "Введите названия удаляемых папок";
read del_folders;
del_folders=( `echo "$del_folders"` ); 
num_del_folders=${#del_folders[@]};
cd $1; 
for (( j=1; j<=$num_folders_level1; j++ ))
do
	cd $j;
	for (( i=0; i<=$num_del_folders; i++ ))
	do
		rm -rf ${del_folders[i]};
	done
	cd ..;
done
echo "Папки второго уровня были удалены";
fi
#удаление конкретных файлов на втором уровне
if (($item==7))
then
echo "Введите названия удаляемых файлов";
read del_files;
del_files=( `echo "$del_files"` ); 
echo "удаляемые файлы " ${del_files[@]};
num_del_files=${#del_files[@]};
echo "количество удаляемых файлов " ${#del_files[@]};
cd $1;
for (( j=1; j<=$num_folders_level1; j++ ))
do
	cd $j;
	for (( i=1; i<=$num_folders_level2; i++ ))
	do
		cd $i;
		for (( k=0; k<$num_del_files; k++ ))
		do
			rm -rf ${del_files[k]};
		done
		cd ..;
	done
	cd ..;
done
echo "Файлы были удалены";
fi